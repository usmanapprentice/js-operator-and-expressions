function CalculateDistance () {
	let Xpoint = arguments[0]||0, Ypoint = arguments[1]||0, Xcircle = arguments[2]||0, Ycircle = arguments[3]||0;
	return Math.sqrt((Math.pow(Xpoint-Xcircle, 2)) + Math.abs(Math.pow(Ypoint-Ycircle, 2)));
}
function IsInCircle (arr) {
	let X = arr[0], Y = arr[1];
	let Radius = 2;
	return CalculateDistance(X,Y) < Radius;
}
console.log((IsInCircle([1,1]))?"In Circle":"Not in Circle");

