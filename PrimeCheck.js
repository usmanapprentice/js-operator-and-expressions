function IsPrime (n) {
	if (n <= 0) {
		return false;
	}
	let half = Math.floor(n/2);
	while (half > 1) {
		if (n%half == 0) {
			return false;
		}
		half -= 1;
	}
	return true;
}

console.log(IsPrime(1));