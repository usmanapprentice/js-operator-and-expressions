function CalculateAreaPerimeter (x,y) {
	let area = parseFloat(x*y).toFixed(2);
	let perimeter =parseFloat(2*(x+y)/1).toFixed(2);
	console.log(`Area = ${area}, Perimeter = ${perimeter}`);
}
CalculateAreaPerimeter(2.5, 3);