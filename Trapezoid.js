function Trapezoid (array) {
	let a = +array[0], b = +array[1], h = +array[2];
	let AreaTrapezoid = ((a+b)/2)*h;
	return parseFloat(AreaTrapezoid).toFixed(7);
}

console.log(Trapezoid(['5', '7', '12']));